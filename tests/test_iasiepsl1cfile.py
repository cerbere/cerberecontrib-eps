"""

Test class for cerbere IASI EPS L1C mapper

:copyright: Copyright 2016 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from cerberecontrib_eps.mapper.checker import Checker


class IASIL1CEPSFile(Checker, unittest.TestCase):
    """Test class for IASI EPS L1C files"""

    def __init__(self, methodName="runTest"):
        super(IASIL1CEPSFile, self).__init__(methodName)

    @classmethod
    def mapper(cls):
        """Return the mapper class name"""
        return 'iasiepsfile.IASIL1CEPSFile'

    @classmethod
    def datamodel(cls):
        """Return the related datamodel class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "IASI_xxx_1C_M02_20161121150557Z_20161121150853Z_N_O_20161121160711Z"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
