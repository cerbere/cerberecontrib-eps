import unittest

from cerberecontrib_eps.dataset.pfspy.BinaryRecordClass import BinaryRecord
from cerberecontrib_eps.dataset.pfspy.BinBufClass import BinaryBuffer
from cerberecontrib_eps.dataset.pfspy.PfsGlobals import GpfsFormatDict
from cerberecontrib_eps.dataset.pfspy.GrhClass import GrhClass
from cerberecontrib_eps.dataset.pfspy.IprClass import IPR
from cerberecontrib_eps.dataset.pfspy.LongCdsTimeClass import LongCDSTime
from cerberecontrib_eps.dataset.pfspy.MphrClass import MPHR
from cerberecontrib_eps.dataset.pfspy.PointerClass import PointerClass
from cerberecontrib_eps.dataset.pfspy.RecordFormatClass import RecordFormat
from cerberecontrib_eps.dataset.pfspy.ShortCdsTimeClass import ShortCdsTime


class EPSBaseClass(unittest.TestCase):

    def test_BinBufClass(self):
        instance = BinaryBuffer()

        instance.k = 0
        instance.pack('>b', 2)
        print(instance)
        instance.pack('>i', 3)
        print(instance)
        instance.pack('>b, >i', (2, 3))
        print(instance)

        instance.k = 0
        print(instance.s)
        self.assertListEqual(instance.unpack('>b, >i').tolist(), [(2, 3)])
        print(instance)

    def test_GrhClass(self):
        grh = GrhClass()
        self.assertEqual(grh.NBYTES, 20)
        grh.init_values()

    def test_CdsLongTimeClass(self):
        obj = LongCDSTime()

    def test_CdsShortTimeClass(self):
        obj = ShortCdsTime()

    def test_IprClass(self):
        obj = IPR()
        obj.init_values()
        print(obj)

    def test_MphrClass(self):
        print('=======================')
        print('Test MphrClass')
        obj = MPHR()
        obj.init_values()
        print(obj)

    def test_PointerClass(self):
        print('=======================')
        print('Test PointerClass')
        obj = PointerClass()
        obj.init_values()
        print(obj)

    def test_RecordFormatClass(self):
        print('=======================')
        print('Test RecordFormatClass')
        obj = RecordFormat()
        obj.display_def()
        buf = obj.create_buffer()
        k = buf.k
        obj.pack(buf)
        print(obj)
        buf.k = k
        obj.pack(buf)
        print(obj)

    def test_BinaryRecordClass(self):
        print('=======================')
        print('Test BinaryRecordClass')
        obj = BinaryRecord()
        obj.display_def()
        buf = obj.create_buffer()
        k = buf.k
        obj.pack(buf)
        print(obj)
        buf.k = k
        obj.pack(buf)
        print(obj)

    def test_globals(self):
        print("SIZE", 'bitst(8)', (GpfsFormatDict['bitst8']).itemsize)
        print("SIZE", 'bitst(16)', (GpfsFormatDict['bitst16']).itemsize)
        print("SIZE", 'bitst(32)', (GpfsFormatDict['bitst32']).itemsize)
        print("SIZE", 'bitst(48)', (GpfsFormatDict['bitst48']).itemsize)
        print("SIZE", 'bitst(64)', (GpfsFormatDict['bitst64']).itemsize)
        print("SIZE", 'bitst(256)', (GpfsFormatDict['bitst256']).itemsize)
        print("SIZE", 'boolean', (GpfsFormatDict['boolean']).itemsize)
        print("SIZE", 'enumerated', (GpfsFormatDict['enumerated']).itemsize)
        print("SIZE", 'byte', (GpfsFormatDict['byte']).itemsize)
        print("SIZE", 'u-byte', (GpfsFormatDict['u-byte']).itemsize)
        print("SIZE", 'integer2', (GpfsFormatDict['integer2']).itemsize)
        print("SIZE", 'u-integer2', (GpfsFormatDict['u-integer2']).itemsize)
        print("SIZE", 'integer4', (GpfsFormatDict['integer4']).itemsize)
        print("SIZE", 'u-integer4', (GpfsFormatDict['u-integer4']).itemsize)
        print("SIZE", 'v-integer4', (GpfsFormatDict['v-integer4']).itemsize)
        print("SIZE", 'integer8', (GpfsFormatDict['integer8']).itemsize)
        print("SIZE", 'u-integer8', (GpfsFormatDict['u-integer8']).itemsize)
        print("SIZE", 'REC_HEAD', (GpfsFormatDict['REC_HEAD']).itemsize)
        print("SIZE", 'short cds time',
              (GpfsFormatDict['short cds time']).itemsize)
        print("SIZE", 'long cds time',
              (GpfsFormatDict['long cds time']).itemsize)
        print("SIZE", 'general time', (GpfsFormatDict['general time']).itemsize)
        print("SIZE", 'long general time',
              (GpfsFormatDict['long general time']).itemsize)
        print('OK')

if __name__ == '__main__':
    unittest.main()
