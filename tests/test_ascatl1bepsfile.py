"""
Test class for cerbere EPS ASCAT L1B dataset
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import unittest

from tests.checker import Checker


class ASCATL1BEPSDatasetL2PChecker(Checker, unittest.TestCase):
    """Test class for ASCATL1BEPSDataset swath files"""

    def __init__(self, methodName="runTest"):
        super(ASCATL1BEPSDatasetL2PChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the dataset class name"""
        return 'ASCATL1BSZREPSDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "ASCA_SZR_1B_M01_20200130231800Z_20200131005958Z_N_O_20200131000638Z.nat"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"

    def test_dim_swath(self):
        ncf = self.datasetclass(self.testfile)
        swath = self.featureclass(ncf)
        self.assertEquals(
            tuple(swath.get_geocoord('lat').dims.keys()),
            ('row', 'cell',)
        )
        self.assertEquals(
            swath.get_field_dims('sigma0'),
            ('row', 'cell', 'beam')
        )