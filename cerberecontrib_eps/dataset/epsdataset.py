# -*- coding: utf-8 -*-
"""
Dataset class for products in EPS format.
"""

from collections import OrderedDict
import datetime
import logging

import xarray as xr

from cerberecontrib_eps.dataset.pfspy.FileClass import FileClass
from cerberecontrib_eps.dataset.pfspy.RecordTemplate import RecordTemplate
from cerbere.dataset.dataset import Dataset


METOP_EPOCH_TIME = datetime.datetime(2000, 1, 1, 0, 0, 0)


class EPSDataset(Dataset):
    """Mapper class to read products in EPS format.
    """

    ATTRIBUTE_ENTRIES = {}
    SKIPPED_ENTRIES = ['year', 'month', 'day', 'hour', 'minute', 'second']

    FILLVALUE = 1.7e38

    def __init__(self, *args, template=None, **kwargs):
        """
        Initialize a EPS file dataset
        """
        if template is None:
            raise ValueError("A product template must be provided")
        self.template = RecordTemplate(template)
        super(EPSDataset, self).__init__(*args, **kwargs)

    def _get_product_fields(self):
        """return the list of fields in EPS MDR block"""
        return self.template.MDR.keys()

    def _read_header(self, handler):
        """read EPS product header"""
        mphr = handler.read()
        logging.debug('Reading header')
        attrs = OrderedDict([])
        for att, attval in mphr.fields.items():
            attrs[att.lower()] = attval
            logging.debug('%s: %s', att.lower(), attval)

        nbrecords = mphr.fields['TOTAL_RECORDS']

        return attrs, nbrecords

    def _open_dataset(self, **kwargs) -> 'xr.Dataset':
        handler = FileClass(self._url, template=self.template)
        handler.open()

        attrs, nbrecords = self._read_header(handler)
        xrdataset = self._read_data(handler, nbrecords)

        xrdataset.attrs.update(attrs)

        return xrdataset

    def _read_data(self, handler, nbrecords) -> 'xr.Dataset':

        # internal buffers to keep data in memory
        records = []

        geocoords = {}
        data_vars = {}

        # count number of scan lines and read each associated record
        nbscans = 0
        for _ in range(1, nbrecords):
            rec = handler.read(specific=1)
            if rec.is_mdr():
                lastrec = rec
                nbscans += 1
                records.append(rec)

        # get all dataset dimensions
        dims = self._coordinate_dims(nbscans)
        for d in self.template.dims:
            if d not in dims:
                dims[d] = self.template.dims[d]

        # create dataarrays
        for fieldname in self._get_product_fields():

            # create and store a Field object
            fieldobj = lastrec.fields[fieldname]

            field_dims = [self.template.main_dim] + fieldobj.descriptor.broadcast_dims
            units = fieldobj.descriptor.unit

            field = xr.DataArray(
                name=fieldname,
                data=self._unpack(fieldname, records),
                dims=field_dims,
                attrs={
                    'long_name': fieldobj.descriptor.description,
                }
            )
            if units is not None:
                if fieldname == 'time':
                    field.encoding['units'] = units
                else:
                    field.attrs['units'] = units

            if fieldname in ['lat', 'lon', 'time', 'z']:
                geocoords[fieldname] = field
            else:
                data_vars[fieldname] = field

        xrdataset = xr.Dataset(
            coords=geocoords,
            data_vars=data_vars
        )

        return xrdataset

    def _get_record_field_shape(self, fieldname):
        """
        """
        recshape = []
        for d in self.template.MDR[fieldname].broadcast_dims:
            recshape.append(self.template.dims[d])
        return tuple(recshape)
