# -*- coding: utf-8 -*-
"""
Dataset class for ASCAT products in EPS format.
"""
from collections import OrderedDict
import datetime
import logging

import numpy as np

from cerberecontrib_eps.dataset.pfspy.ShortCDSTime import ShortCDSTime
from cerberecontrib_eps.dataset.pfspy.LongCDSTime import LongCDSTime
from cerberecontrib_eps.dataset.epsdataset import EPSDataset

LOGGER = logging.getLogger()


class ASCATEPSDataset(EPSDataset):
    """Dataset class to read ASCAT products in EPS format."""

    def _coordinate_dims(self, nbscans):
        return OrderedDict([
            ('row', nbscans),
            ('cell', self.template.dims['cell'])
            ])

    def _unpack(self, fieldname, records):
        """read the values of a field"""
        scanlines = []

        for rec in [_ for _ in records if _.is_mdr()]:
            # read values of scan line and convert to numpy type
            dtype = rec.fields[fieldname].descriptor.dtype
            values = np.ma.array(
                rec.fields[fieldname].value.astype(dtype),
                copy=False
            )

            recshape = self._get_record_field_shape(fieldname)
            if len(recshape) > 0 and values.shape != recshape:
                if len(recshape) < len(values.shape):
                    raise ValueError(
                        "something wrong with the record and expected dims:"
                        " {} vs {}".format(values.shape, recshape)
                    )
                values = np.ma.resize(values, recshape)

            # add reconstructed scan lines
            scanlines.append(values)

        try:
            values = np.ma.masked_equal(
                np.stack(scanlines), rec.fields[fieldname].descriptor.fill_value
                )
            values = values.reshape(tuple([len(scanlines)] + list(recshape)))

        except TypeError:
            logging.error(
                "fill_value {} not correct for type {} in {}".format(
                    values.dtype,
                    rec.fields[fieldname].descriptor.fill_value,
                    fieldname
                )
            )
            raise

        # swap cell dimension to come immediately after row
        dims = rec.fields[fieldname].descriptor.dims
        if 'cell' in dims and len(dims) > 2:
            idx = dims.index('cell')
            if idx != 1:
                values = values.swapaxes(idx, 1)

        # shift longitudes
        if fieldname == 'lon':
            values[values > 180] -= 360.

        return values


class ASCATL1BSZREPSDataset(ASCATEPSDataset):
    """Dataset class to read L1B ASCAT products in EPS format.
    """
    def __init__(self, *args, **kwargs):
        super(ASCATL1BSZREPSDataset, self).__init__(
            *args, template="ASCAT_SZR.rec", **kwargs
            )


class ASCATL1BSZFEPSDataset(ASCATEPSDataset):
    """Dataset class to read L1B ASCAT SZF products in EPS format.
    """
    def __init__(self, *args, **kwargs):
        super(ASCATL1BSZFEPSDataset, self).__init__(
            *args, template="ASCAT_SZF.rec", **kwargs
            )