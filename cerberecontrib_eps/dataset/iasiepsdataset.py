# -*- coding: utf-8 -*-
"""
.. module::cerbere.dataset.iasiepsfile

Mapper classs for IASI products in EPS format.

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from collections import OrderedDict
import datetime
import logging

import numpy
import netCDF4

from cerberecontrib_eps.dataset.iasipy.L1 import FileClass
from cerberecontrib_eps.dataset.abstractmapper import AbstractMapper
from cerberecontrib_eps.datamodel.field import Field
from cerberecontrib_eps.datamodel.variable import Variable
import cerberecontrib_eps.dataset.slices


METOP_EPOCH_TIME = datetime.datetime(2000, 1, 1, 0, 0, 0)


class IASIEPSFile(AbstractMapper):
    """Mapper class to read IASI products in EPS format.
    """

    FILLVALUE = 1.7e38

    # define here the friendly field name and related native field name,
    # dimensions and type of the field.

    # Look in MdrL1... classes for the field descriptions. Reverse the
    # dimension order (ex: dim4, dim3, dim2, dim1)
    #
    # in dimensions, use dimension name 'INTERLACED' for fields values
    # interlaced into a single field (ex; lat/lon in GeoLoc)
    IASI_FIELDS = OrderedDict([
        ('lat', ('GGeoSondLoc', ('SNOT', 'PN', 'INTERLACED'), numpy.float32)),
        ('lon', ('GGeoSondLoc', ('SNOT', 'PN', 'INTERLACED'), numpy.float32)),
        ('time', ('GEPSDatIasi', ('SNOT',), numpy.int32)),
        ('satellite_zenith_angle', ('GGeoSondAnglesMETOP',
                                    ('SNOT', 'PN', 'INTERLACED'),
                                    numpy.float32)),
        ('satellite_azimuth_angle', ('GGeoSondAnglesMETOP',
                                     ('SNOT', 'PN', 'INTERLACED'),
                                     numpy.float32)),
        ('sun_zenith_angle', ('GGeoSondAnglesSUN',
                              ('SNOT', 'PN', 'INTERLACED'),
                              numpy.float32
                              )),
        ('sun_azimuth_angle', ('GGeoSondAnglesSUN',
                               ('SNOT', 'PN', 'INTERLACED'),
                               numpy.float32
                               )),
        ('detailed_quality_flag',
         ('GQisFlagQualDetailed', ('SNOT', 'PN'), numpy.uint16)
         ),
    ])

    INTERLACED_VAR_FIRST = ['satellite_zenith_angle',
                            'sun_zenith_angle',
                            'lon']
    INTERLACED_VAR_LAST = ['satellite_azimuth_angle',
                           'sun_azimuth_angle',
                           'lat']

    def __init__(self, url=None, **kwargs):
        """
        Initialize a EPS file dataset
        """
        super(IASIEPSFile, self).__init__(url=url, **kwargs)
        self.__globalattributes = None
        self.__records = []
        self.__std2native = {}
        self.__native2std = {}
        self.__nativedimensions = {}
        self.__types = {}
        self._fields = {}
        self._geofields = {}
        for stdfield, _ in self._get_product_fields().items():
            natfield, dims, ftype = _
            self.__std2native[stdfield] = natfield
            self.__native2std[natfield] = stdfield
            self.__nativedimensions[stdfield] = dims
            self.__types[stdfield] = ftype

        return

    def _get_product_fields(self):
        """return the list if fields managed by the dataset.

        TO BE OVERRIDEN IN ANY DERIVED CLASS
        """
        return self.IASI_FIELDS

    def __get_pretty_fieldname(self, epsfieldname):
        """Return a more explict fieldname than the internal one"""
        if epsfieldname in self.__native2std:
            return self.__native2std[epsfieldname]
        else:
            return epsfieldname

    def get_geolocation_field(self, fieldname):
        """Return the equivalent field name in the file format for a standard
        geolocation field (lat, lon, time, z).

        Used for internal purpose and should not be called directly.

        Args:
            fieldname (str): name of the standard geolocation field (lat, lon
                or time)

        Return:
            str: name of the corresponding field in the native file format.
                Returns None if no matching is found
        """
        return fieldname

    def get_matching_dimname(self, dimname):
        """Return the equivalent name in the native format for a standard
        dimension.

        This is a translation of the standard names to native ones. As BUFR
        does not have explicit dimensions, we return the standard dimension
        names, which are :

        * row, cell, time for :class:`~cerbere.datamodel.swath.Swath` or
          :class:`~cerbere.datamodel.image.Image`

        Args:
            dimname (str): standard dimension name.

        Returns:
            str: return the native name for the dimension. Return `dimname` if
                the input dimension has no standard name.

        See Also:
            see :func:`get_standard_dimname` for the reverse operation
        """
        return dimname

    def get_standard_dimname(self, dimname):
        """
        Returns the equivalent standard dimension name for a
        dimension in the native format.

        This is a translation of the native names to standard ones. As BUFR
        does not have explicit dimensions, we return the standard dimension
        names, which are :

        Args:
            dimname (string): native dimension name

        Return:
            str: the (translated) standard name for the dimension. Return
            `dimname` if the input dimension has no standard name.

        See Also:
            see :func:`get_matching_dimname` for the reverse operation
        """
        return dimname

    def _make_2d_field(self, shortname, description, unit, dims=None, datatype=numpy.float32):
        """create a field"""
        if dims is None:
            dims = self._dimensions
        varobj = Variable(shortname=shortname, description=description)
        newfield = Field(
            variable=varobj,
            dimensions=dims,
            datatype=datatype,
            fillvalue=netCDF4.default_fillvals[numpy.dtype(datatype).str[1:]],
            units=unit,
        )
        newfield.attach_storage(self.get_field_handler(shortname))
        return newfield

    def open(self, view=None, **kwargs):
        """Open the file (or any other type of storage)

        Args:
            view (dict, optional): a dictionary where keys are dimension names
                and values are slices. A view can be set on a file, meaning
                that only the subset defined by this view will be accessible.
                Any later use of slices in :func:`read_values` will be relative
                to the view defined here.

                This view is expressed as any subset (see :func:`read_values`).
                For example:

                .. code-block: python

                    view = {'time':slice(0,0), 'row':slice(200,300),
                        'cell':slice(200,300)}
        Returns:
            an handler on the opened file
        """
        # open file
        self._handler = FileClass(self._url, verbose=0)
        self._handler.open()
        super(IASIEPSFile, self).open(view=view,
                                      datamodel='Swath')

        # read global attributes
        mphr = self._handler.read()
        logging.debug('Header')
        self.__globalattributes = OrderedDict([])
        for att in mphr.FieldList:
            attval = mphr.__dict__[att]
            self.__globalattributes[att.lower()] = attval
            logging.debug('%s: %s', att.lower(), attval)
        self.__globalattributes.update(OrderedDict([
            ('institution', 'EUMETSAT'),
            ('institution_abbreviation', 'EUMETSAT'),
            ('publisher_name', 'EUMETSAT'),
            ('publisher_url', 'http://eumetsat.int'),
            ('publisher_email', 'ops@eumetsat.int'),
            ('creator_name', 'EUMETSAT'),
            ('creator_url', 'http://eumetsat.int'),
            ('creator_email', 'ops@eumetsat.int'),
            ('references', 
             'IASI Level 1 Product Format Specification, '
             'EUM.EPS.SYS.SPE.990003, v9E, 14 Dec 2011'),
            ('sensor', self.__globalattributes.pop('instrument_id')),
            ('sensor_type', 'fourier transform spectrometer'),
            ('band', '15.5 - 3.62 um'),
            ('platform', {
                'M01': 'MetOp-A',
                'M02': 'MetOp-B'}[
                    self.__globalattributes.pop('spacecraft_id')]),
            ('source', self.__globalattributes.pop('product_name')),
            ('product_version', 
             '{}.{}'.format(
                 self.__globalattributes.pop('processor_major_version'),
                 self.__globalattributes.pop('processor_minor_version'))),
            ('format_version', 
             '{}.{}'.format(
                 self.__globalattributes.pop('format_major_version'),
                 self.__globalattributes.pop('format_minor_version'))),
            ('platform_type', 'leo satellite')                                             
            ])
        )

        # count number of scan lines
        nbscans = 0
        for _ in range(1, mphr.TOTAL_RECORDS):
            # with spectra
            rec = self._handler.read(specific=1)
            if rec.is_giadr() and rec.prefix == 'GIADR-SCALEFACTORS':
                self.scalefactor = rec

            elif rec.is_mdr() and rec.record_subclass == 2:
                lastrec = rec
                nbscans += 1
                self.__records.append(rec)

        # get coordinate dimensions (take into account the view)
        self._dimensions = OrderedDict([
            ('row', nbscans * 2),
            ('cell', lastrec.SNOT * 2)
        ])

        # verify assumption on subpixels
        assert(lastrec.PN == 4)

        # create fields for the implemented selection
        for fieldname in self._get_product_fields():
            nativename = self._get_native_fieldname(fieldname)
            dimnames = self.__nativedimensions[fieldname]

            # skip interlaced fields
            if 'INTERLACED' in dimnames:
                continue

            # skip coordinate fields
            if fieldname in ['time', 'lat', 'lon']:
                continue

            # create and store a Field object
            fieldobj = lastrec.__dict__[nativename]
            ftype = self.__types[fieldname]
            dimensions = OrderedDict([])
            if len(dimnames) != 0:
                dimensions['row'] = self._dimensions['row']

            if 'SNOT' in dimnames:
                dimensions['cell'] = self._dimensions['cell']
            for dim in dimnames:
                if dim == 'SNOT' or dim == 'PN':
                    continue
                dimsize = lastrec.__dict__[dim]
                stddim = self.get_standard_dimname(dim)
                dimensions[stddim] = dimsize
                if not dim in self.__nativedimensions:
                    self.__nativedimensions[dim] = dimsize
                elif dimsize != self.__nativedimensions[dim]:
                    raise Exception(
                        'Unconsistent dimension size in EPS file for {}'
                        .format(dim)
                    )

            varobj = Variable(fieldname, description=fieldobj.description)
            field = Field(
                variable=varobj,
                dimensions=dimensions,
                datatype=ftype,
                fillvalue=netCDF4.default_fillvals[
                    numpy.dtype(self.__types[fieldname]).str[1:]
                    ],
                units=fieldobj.unit,
            )
            field.attach_storage(self.get_field_handler(fieldname))

            self._fields[fieldname] = field

        # special interlaced fields

        # geolocation
        self._geofields['lat'] = self._make_2d_field('lat', 'latitude',
                                                     'degrees_north')
        self._geofields['lon'] = self._make_2d_field('lon', 'longitude',
                                                     'degrees_east')
        timeunit = ('seconds since %s'
                    % datetime.datetime.strftime(METOP_EPOCH_TIME,
                                                 "%Y-%m-%d %H:%M:%S"))
        self._geofields['time'] = self._make_2d_field(
            'time', 'time of measurement',
            unit=timeunit,
            datatype=self.IASI_FIELDS['time'][2]
            )
        # HARD CODE HERE FIELD CREATION

        # angles
        self._fields['satellite_zenith_angle'] = self._make_2d_field(
            'satellite_zenith_angle',
            'satellite zenith angle',
            'degrees',
            datatype=self.IASI_FIELDS['satellite_zenith_angle'][2]
            )
        self._fields['satellite_azimuth_angle'] = self._make_2d_field(
            'satellite_azimuth_angle',
            'satellite azimuth angle',
            'degrees',
            datatype=self.IASI_FIELDS['satellite_azimuth_angle'][2]
            )
        self._fields['sun_zenith_angle'] = self._make_2d_field(
            'sun_zenith_angle',
            'sun zenith angle',
            'degrees',
            datatype=self.IASI_FIELDS['sun_zenith_angle'][2]
            )
        self._fields['sun_azimuth_angle'] = self._make_2d_field(
            'sun_azimuth_angle',
            'sun azimuth angle',
            'degrees',
            datatype=self.IASI_FIELDS['sun_azimuth_angle'][2]
            )

        return self._handler

    def close(self):
        """Close handler on storage"""
        del self._handler
        self._handler = None

    def _get_record_info(self, info):
        """Return the value of a particular item in the EPS record structure"""
        rec = self.__records[-1]
        try:
            return rec.__dict__[info]
        except:
            raise ValueError("Unknown %s item" % info)

    def _get_native_fieldname(self, fieldname):
        try:
            return self.__std2native[fieldname]
        except:
            return fieldname

    def _get_dims(self, fieldname, rec):
        dims = self.__nativedimensions[fieldname]
        fulldims = OrderedDict([])
        for dim in dims:
            if dim == 'INTERLACED':
                dimsize = 2
            else:
                dimsize = rec.__dict__[dim]
            fulldims[dim] = dimsize
        return fulldims

    def _is_subpixel_field(self, fieldname):
        return 'PN' in self.__nativedimensions[fieldname]

    def __degroup(self, fieldname, values, dims):
        """
        Reconstruct scan lines from 4-subpixel groups
        TO BE CHECKED! is the pixel order constant ????
        """
        if not 'PN' in dims:
            return values
        slices1 = []
        slices2 = []
        newshape = []

        for d, dimsize in dims.items():
            if d == 'PN':
                # pixels are grouped by 4, 2 on first scan line and 2 on 2nd
                # scan line
                slices1.append([3, 0])
                slices2.append([2, 1])

            else:
                # reshape to reconstruct a full scan line
                if d == 'SNOT':
                    newshape.append(dimsize * 2)
                else:
                    newshape.append(dimsize)

                slices1.append(slice(None, None))
                slices2.append(slice(None, None))

        return [values[tuple(slices1)].reshape(tuple(newshape)),
                values[tuple(slices2)].reshape(tuple(newshape))]

    def _read(self, fieldname):
        """read the values of a EPS variable"""
        nname = self._get_native_fieldname(fieldname)
        scanlines = []

        for rec in self.__records:
            if rec.is_mdr() and rec.record_subclass == 2:

                # read values of scan line
                if fieldname != 'time':
                    dims = self._get_dims(fieldname, rec)
                    values = rec.__dict__[nname].value

                    if (len(dims) == 0 and
                            isinstance(values[0], numpy.int32)):
                        values = values[0].astype(self.__types[fieldname])

                    elif len(dims) == 0 and len(values[0]) == 2:
                        # case v-integer4, real with sclale factor as power of
                        # ten
                        values = values[0][1] * (10.**-values[0][0])

                    else:
                        values = numpy.ma.array(
                            values, copy=False,
                            dtype=self.__types[fieldname]
                            )
                        dims = self._get_dims(fieldname, rec)
                        values = values.reshape(dims.values())

                # re-arrange data
                if fieldname in self.INTERLACED_VAR_FIRST:
                    # reconstruct scan lines for subpixel groups
                    # special case for interlaced variables
                    trimdims = OrderedDict([(dim, val)
                                            for dim, val in dims.items()[:-1]])
                    values = self.__degroup(
                        fieldname, values[..., 0], trimdims
                    )

                elif fieldname in self.INTERLACED_VAR_LAST:
                    trimdims = OrderedDict([(dim, val)
                                            for dim, val in dims.items()[:-1]])
                    values = self.__degroup(
                        fieldname, values[..., 1], trimdims)

                elif fieldname == 'time':
                    values = numpy.ma.array(
                        [_.EpochDay * 86400. + _.DayMilliSecond / 1000.
                         for _ in rec.GEPSDatIasi.value])

                    values = numpy.ma.resize(values, (values.size * 2,))
                    values = [values, values]
                else:
                    # deconstruct groups of 4 pixels to use scanlines and cell
                    # instead
                    values = self.__degroup(fieldname, values, dims)

                # add reconstructed scan lines
                if type(values) is list:
                    scanlines.extend(values)
                else:
                    scanlines.append(values)

        # print scanlines
        if isinstance(self.__types[fieldname], str):
            values = numpy.stack(scanlines)
        else:
            values = numpy.ma.masked_equal(
                numpy.stack(scanlines),
                netCDF4.default_fillvals[
                        numpy.dtype(self.__types[fieldname]).str[1:]
                        ]
                )

        return values

    def read_values(self, fieldname, slices=None):
        """Read the data of a field.

        Args:
            fieldname (str): name of the field which to read the data from

            slices (list of slice, optional): list of slices for the field if
                subsetting is requested. A slice must then be provided for each
                field dimension. The slices are relative to the opened view
                (see :func:open) if a view was set when opening the file.

        Return:
            MaskedArray: array of data read. Array type is the same as the
                storage type.
        """
        # slice combination
        fulldims = self.get_full_dimensions(fieldname)

        newslices = cerberecontrib_eps.dataset.slices.get_absolute_slices(
            self.view,
            slices=slices,
            dimnames=fulldims.keys(),
            dimsizes=fulldims.values())


        # check if already in buffer
        if fieldname in ['lat', 'lon', 'time']:
            field = self._geofields[fieldname]
        elif fieldname in self._fields:
            field = self._fields[fieldname]
        else:
            raise ValueError("Field {} not defined".format(fieldname))

        # read and bufferize new data
        values = field._values
        if values is None:
            values = self._read(fieldname)
            field.init_values(values)

        # subsetting
        if slices is None:
            return values
        else:
            return values[tuple(newslices)]

    def read_field(self, fieldname):
        """
        Return the :class:`cerbere.field.Field` object corresponding to
        the requested fieldname.

        The :class:`cerbere.field.Field` class contains all the metadata
        describing a field (equivalent to a variable in netCDF).

        Args:
            fieldname (str): name of the field

        Returns:
            :class:`cerbere.field.Field`: the corresponding field object
        """
        if fieldname in ['lat', 'lon', 'time']:
            return self._geofields[fieldname]
        else:
            return self._fields[fieldname]

    def get_fieldnames(self):
        """Returns the list of geophysical fields stored for the feature.

        The geolocation field names are excluded from this list.

        Returns:
            list<string>: list of field names
        """
        return self._fields.keys()

    def get_dimensions(self, fieldname=None):
        """Return the dimension's standard names of a file or a field in the
        file.

        Args:
            fieldname (str): the name of the field from which to get the
                dimensions. For a geolocation field, use the cerbere standard
                name (time, lat, lon), though native field name will work too.

        Returns:
            tuple<str>: the standard dimensions of the field or file.
        """
        if fieldname is None:
            return tuple(self._dimensions.keys())
        elif fieldname in self._fields.keys():
            return self._fields[fieldname].dimensions
        elif fieldname in self._geofields.keys():
            return self._geofields[fieldname].dimensions
        else:
            raise ValueError("Unknown field %s" % fieldname)

    def get_dimsize(self, dimname):
        """Return the size of a dimension.

        Args:
            dimname (str): name of the dimension.

        Returns:
            int: size of the dimension.
        """
        try:
            dimsize = self._dimensions[dimname]
        except:
            dimsize = self._get_record_info(dimname)
        return cerberecontrib_eps.dataset.slices.adjust_dimsize(
            self.view, dimname, dimsize
        )

    def read_field_attributes(self, fieldname):
        """Return the specific attributes of a field.

        Args:
            fieldname (str): name of the field.

        Returns:
            dict<string, string or number or datetime>: a dictionary where keys
                are the attribute names.
        """
        return self._fields[fieldname].attributes

    def read_global_attributes(self):
        """Returns the names of the global attributes.

        Returns:
            list<str>: the list of the attribute names.
        """
        return self.__globalattributes.keys()

    def read_global_attribute(self, name):
        """Returns the value of a global attribute.

        Args:
            name (str): name of the global attribute.

        Returns:
            str, number or datetime: value of the corresponding attribute.
        """
        try:
            return self.__globalattributes[name]
        except:
            raise ValueError("The attribute %s does not exist" % name)

    def get_start_time(self):
        """Returns the minimum date of the file temporal coverage.

        Returns:
            datetime: start time of the data in file.
        """
        return netCDF4.num2date(
            self._geofields['time'].get_values().min(),
            self._geofields['time'].units
        )

    def get_end_time(self):
        """Returns the maximum date of the file temporal coverage.

        Returns:
            datetime: end time of the data in file.
        """
        return netCDF4.num2date(
            self._geofields['time'].get_values().max(),
            self._geofields['time'].units)

    def get_product_version(self):
        """return the product version"""
        return self.__globalattributes['product_version']

    def get_orbit_number(self):
        """In the case of a satellite orbit file, returns the orbit number.

        Returns:
            int: the orbit number
        """
        return int(self.__globalattributes['start_orbit'])

    def write_field(self, fieldname):
        """Writes the field data on disk.

        Args:
            fieldname (str): name of the field to write.
        """
        raise NotImplementedError

    def create_field(self, field, dim_translation=None):
        """Creates a new field in the dataset.

        Creates the field structure but don't write yet its values array.

        Args:
            field (Field): the field to be created.

        See also:
            :func:`write_field` for writing the values array.
        """
        raise NotImplementedError

    def create_dim(self, dimname, size=None):
        """Add a new dimension.

        Args:
            dimname (str): name of the dimension.
            size (int): size of the dimension (unlimited if None)
        """
        raise NotImplementedError

    def write_global_attributes(self, attrs):
        """Write the global attributes of the file.

        Args:
            attrs (dict<string, string or number or datetime>): a dictionary
                containing the attributes names and values to be written.
        """
        raise NotImplementedError

    def get_bbox(self):
        """Returns the bounding box of the feature, as a tuple.

        Returns:
            tuple: bbox expressed as (lonmin, latmin, lonmax, latmax)
        """
        return (self.read_values('lon').min(),
                self.read_values('lat').min(),
                self.read_values('lon').max(),
                self.read_values('lat').max())

    def read_fillvalue(self, fieldname):
        """Read the fill value of a field.

        Args:
            fieldname (str): name of the field.

        Returns:
            number or char or str: fill value of the field. The type is the
                as the type of the data in the field.
        """
        return self.__types[fieldname]()


class IASIL1CEPSFile(IASIEPSFile):
    """Mapper class to read L1C IASI products in EPS format.
    """

    # define here the friendly field name and related native field name,
    # dimensions and type of the field for this product.

    # Look in MdrL1... classes for the field descriptions. Reverse the
    # dimension order (ex: dim4, dim3, dim2, dim1)

    L1C_IASI_FIELDS = OrderedDict([
        ('radiances', ('GS1cSpect', ('SNOT', 'PN', 'SS'), numpy.float64)),
        ('avhrr_cloud_coverage', ('GEUMAvhrr1BCldFrac', ('SNOT', 'PN'), numpy.int8)),
        ('avhrr_land_coastal_coverage', ('GEUMAvhrr1BCldFrac', ('SNOT', 'PN'), numpy.int8)),
        ('avhrr_quality_indicator', ('GEUMAvhrr1BQual', ('SNOT', 'PN'), 'c')),
        ('i_def_spect_dwn1b', ('DefSpectDWn1b', (), numpy.byte)),
        ('i_def_ns_first_1b', ('IDefNsfirst1b', (), numpy.int32)),
        ('i_def_ns_last_1b', ('IDefNslast1b', (), numpy.int32)),
    ])

    HIDDEN_FIELDS = ['i_def_spect_dwn1b',
                     'i_def_ns_first_1b',
                     'i_def_ns_last_1b']

    def __init__(self, url=None, **kwargs):
        """
        Initialize a EPS file dataset
        """
        self.IASI_FIELDS.update(self.L1C_IASI_FIELDS)
        super(IASIL1CEPSFile, self).__init__(url=url, **kwargs)

    def open(self, view=None, **kwargs):
        IASIEPSFile.open(self, view=view, **kwargs)

        # add virtual field wavenumber
        varobj = Variable(shortname='wavenumber',
                          description="wave number of spectrum samples")
        datatype = numpy.float32
        field = Field(
            variable=varobj,
            dimensions=OrderedDict([('spectrum_sample', self._get_record_info('SS'))]),
            datatype=numpy.float32,
            fillvalue=netCDF4.default_fillvals[numpy.dtype(datatype).str[1:]],
            units='m-1',
        )
        field.attach_storage(self.get_field_handler('wavenumber'))
        self._fields['wavenumber'] = field

    def read_values(self, fieldname, slices=None):
        if fieldname == 'wavenumber':
            # w(k)= IDefSpectDWn1b * (IDefNsfirst1b+k-2)
            scale = IASIEPSFile._read(self, 'i_def_spect_dwn1b')[0]
            # convert to m-1
            scale *= 100. 
            offset = IASIEPSFile._read(self, 'i_def_ns_first_1b')[0]
            wavenumber = scale * \
                (offset + numpy.arange(self.get_dimsize('SS')) - 2)
            if slices is not None and 'SS' in slices:
                wavenumber = wavenumber[slices['SS']]
            return wavenumber

        else:
            values = super(IASIL1CEPSFile, self).read_values(fieldname, slices=slices)
            if fieldname == 'radiances':

                # apply scale factor to spectral samples
                offset = IASIEPSFile._read(self, 'i_def_ns_first_1b')[0]
                scale = numpy.ma.zeros((self.get_dimsize('SS')), numpy.float64)
                for k in range(self.scalefactor.IDefScaleSondNbScale.value):
                    start = self.scalefactor.IDefScaleSondNsfirst.value[k] - offset
                    end = self.scalefactor.IDefScaleSondNslast.value[k] -offset + 1
                    if start >= self.get_dimsize('SS'):
                        break
                    scale[start:end] = numpy.float64(10)**-self.scalefactor.IDefScaleSondScaleFactor.value[k]
                values *= scale                

            return values

    def get_fieldnames(self):
        fields = self._fields.keys()
        fields = [_ for _ in fields if _ not in self.HIDDEN_FIELDS]
        return fields

        
    def get_matching_dimname(self, dimname):
        """Return the equivalent name in the native format for a standard
        dimension.

        This is a translation of the standard names to native ones. As BUFR
        does not have explicit dimensions, we return the standard dimension
        names, which are :

        * row, cell, time for :class:`~cerbere.datamodel.swath.Swath` or
          :class:`~cerbere.datamodel.image.Image`

        Args:
            dimname (str): standard dimension name.

        Returns:
            str: return the native name for the dimension. Return `dimname` if
                the input dimension has no standard name.

        See Also:
            see :func:`get_standard_dimname` for the reverse operation
        """
        if dimname == 'spectrum_sample':
            return 'SS'
        return dimname

    def get_standard_dimname(self, dimname):
        """
        Returns the equivalent standard dimension name for a
        dimension in the native format.

        This is a translation of the native names to standard ones. As BUFR
        does not have explicit dimensions, we return the standard dimension
        names, which are :

        Args:
            dimname (string): native dimension name

        Return:
            str: the (translated) standard name for the dimension. Return
            `dimname` if the input dimension has no standard name.

        See Also:
            see :func:`get_matching_dimname` for the reverse operation
        """
        if dimname == 'SS':
            return 'spectrum_sample'
        return dimname
