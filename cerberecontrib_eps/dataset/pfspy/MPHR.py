"""
GPFS Main Product Header Record Class
"""
import numpy as np

from .PFSRecord import *
from .GeneralTimeClass import GeneralTimeClass

FIELDS = {
    'INSTRUMENT_ID': 'x',
    'PRODUCT_TYPE': 'x',
    'RECEIVING_GROUND_STATION': 'X',
    'PARENT_PRODUCT_NAME_2': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    'PARENT_PRODUCT_NAME_3': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    'PARENT_PRODUCT_NAME_1': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    'INSTRUMENT_MODEL': 0,
    'PARENT_PRODUCT_NAME_4': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    'RIGHT_ASCENSION': 0,
    'SUBSAT_LATITUDE_END': 0,
    'PROCESSING_MODE': 'X',
    'DISPOSITION_MODE': 'X',
    'SUBSETTED_PRODUCT': 'F',
    'ACTUAL_PRODUCT_SIZE': 0,
    'ECCENTRICITY': 0,
    'PROCESSOR_MINOR_VERSION': 0,
    'Y_POSITION': 7400000000,
    'SENSING_START_THEORETICAL': 'xxxxxxxxxxxxxxZ',
    'PRODUCT_NAME': 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    'SUBSAT_LONGITUDE_END': 0,
    'SUBSAT_LATITUDE_START': 0,
    'LOCATION_TOLERANCE_RADIAL': 0,
    'MILLISECONDS_OF_DATA_PRESENT': 0,
    'SENSING_END': 'xxxxxxxxxxxxxxZ',
    'Z_VELOCITY': 0,
    'COUNT_DEGRADED_INST_MDR_BLOCKS': 0,
    'PITCH_ERROR': 0,
    'SENSING_START': 'xxxxxxxxxxxxxxZ',
    'X_VELOCITY': 0,
    'SPACECRAFT_ID': 'x',
    'TOTAL_VIADR': 0,
    'PROCESSOR_MAJOR_VERSION': 0,
    'X_POSITION': 7400000000,
    'ORBIT_START': 0,
    'TOTAL_VEADR': 0,
    'SENSING_END_THEORETICAL': 'xxxxxxxxxxxxxxZ',
    'TOTAL_RECORDS': 0,
    'COUNT_DEGRADED_PROC_MDR': 0,
    'DURATION_OF_PRODUCT': 0,
    'TOTAL_IPR': 0,
    'COUNT_DEGRADED_PROC_MDR_BLOCKS': 0,
    'FORMAT_MINOR_VERSION': 0,
    'SEMI_MAJOR_AXIS': 7400000000,
    'TOTAL_MDR': 0,
    'FORMAT_MAJOR_VERSION': 0,
    'RECEIVE_TIME_END': 'xxxxxxxxxxxxxxZ',
    'MILLISECONDS_OF_DATA_MISSING': 0,
    'STATE_VECTOR_TIME': 'xxxxxxxxxxxxxxxxxZ',
    'PROCESSING_TIME_END': 'xxxxxxxxxxxxxxZ',
    'Y_VELOCITY': 0,
    'SUBSAT_LONGITUDE_START': 0,
    'PROCESSING_TIME_START': 'xxxxxxxxxxxxxxZ',
    'MEAN_ANOMALY': 0,
    'LOCATION_TOLERANCE_ALONGTRACK': 0,
    'TOTAL_GIADR': 0,
    'EARTH_SUN_DISTANCE_RATIO': 0,
    'PROCESSING_LEVEL': 'x',
    'INCLINATION': 0,
    'LOCATION_TOLERANCE_CROSSTRACK': 0,
    'TOTAL_MPHR': 0,
    'YAW_ERROR': 0,
    'TOTAL_GEADR': 0,
    'PROCESSING_CENTRE': 'xxxx',
    'PERIGEE_ARGUMENT': 0,
    'COUNT_DEGRADED_INST_MDR': 0,
    'TOTAL_SPHR': 0,
    'RECEIVE_TIME_START': 'xxxxxxxxxxxxxxZ',
    'ORBIT_END': 0,
    'ROLL_ERROR': 0,
    'Z_POSITION': 0,
    'LEAP_SECOND_UTC': 'xxxxxxxxxxxxxxZ',
    'LEAP_SECOND': 0
}

FIELD_SIZES = {
            'INSTRUMENT_ID': 37,
            'PRODUCT_TYPE': 36,
            'RECEIVING_GROUND_STATION': 36,
            'PARENT_PRODUCT_NAME_2': 100,
            'PARENT_PRODUCT_NAME_3': 100,
            'PARENT_PRODUCT_NAME_1': 100,
            'INSTRUMENT_MODEL': 36,
            'PARENT_PRODUCT_NAME_4': 100,
            'RIGHT_ASCENSION': 44,
            'SUBSAT_LATITUDE_END': 44,
            'PROCESSING_MODE': 34,
            'DISPOSITION_MODE': 34,
            'SUBSETTED_PRODUCT': 34,
            'ACTUAL_PRODUCT_SIZE': 44,
            'ECCENTRICITY': 44,
            'PROCESSOR_MINOR_VERSION': 38,
            'Y_POSITION': 44,
            'SENSING_START_THEORETICAL': 48,
            'PRODUCT_NAME': 100,
            'SUBSAT_LONGITUDE_END': 44,
            'SUBSAT_LATITUDE_START': 44,
            'LOCATION_TOLERANCE_RADIAL': 44,
            'MILLISECONDS_OF_DATA_PRESENT': 41,
            'SENSING_END': 48,
            'Z_VELOCITY': 44,
            'COUNT_DEGRADED_INST_MDR_BLOCKS': 39,
            'PITCH_ERROR': 44,
            'SENSING_START': 48,
            'X_VELOCITY': 44,
            'SPACECRAFT_ID': 36,
            'TOTAL_VIADR': 39,
            'PROCESSOR_MAJOR_VERSION': 38,
            'X_POSITION': 44,
            'ORBIT_START': 38,
            'TOTAL_VEADR': 39,
            'SENSING_END_THEORETICAL': 48,
            'TOTAL_RECORDS': 39,
            'COUNT_DEGRADED_PROC_MDR': 39,
            'DURATION_OF_PRODUCT': 41,
            'TOTAL_IPR': 39,
            'COUNT_DEGRADED_PROC_MDR_BLOCKS': 39,
            'FORMAT_MINOR_VERSION': 38,
            'SEMI_MAJOR_AXIS': 44,
            'TOTAL_MDR': 39,
            'FORMAT_MAJOR_VERSION': 38,
            'RECEIVE_TIME_END': 48,
            'MILLISECONDS_OF_DATA_MISSING': 41,
            'STATE_VECTOR_TIME': 51,
            'PROCESSING_TIME_END': 48,
            'Y_VELOCITY': 44,
            'SUBSAT_LONGITUDE_START': 44,
            'PROCESSING_TIME_START': 48,
            'MEAN_ANOMALY': 44,
            'LOCATION_TOLERANCE_ALONGTRACK': 44,
            'TOTAL_GIADR': 39,
            'EARTH_SUN_DISTANCE_RATIO': 44,
            'PROCESSING_LEVEL': 35,
            'INCLINATION': 44,
            'LOCATION_TOLERANCE_CROSSTRACK': 44,
            'TOTAL_MPHR': 39,
            'YAW_ERROR': 44,
            'TOTAL_GEADR': 39,
            'PROCESSING_CENTRE': 37,
            'PERIGEE_ARGUMENT': 44,
            'COUNT_DEGRADED_INST_MDR': 39,
            'TOTAL_SPHR': 39,
            'RECEIVE_TIME_START': 48,
            'ORBIT_END': 38,
            'ROLL_ERROR': 44,
            'Z_POSITION': 44,
            'LEAP_SECOND_UTC': 48,
            'LEAP_SECOND': 35
}


class MPHR(PFSRecord):
    """
    GPFS Main Product Header Record Class
    """
    def __init__(self):
        PFSRecord.__init__(self)

        self.display_list = [self.__class__.__name__]
        self.record_class = 1

        self.fields = FIELDS
        self.field_size = FIELD_SIZES

    def unpack(self, buf):
        items = np.where(buf.s == b'\n')
        buf.k = self.GRH.nbytes

        for item in items[0]:
            line = buf.s[buf.k:item].tostring().decode('utf-8')
            attr, vstr = [_.strip() for _ in line.split('=')]
            buf.k = item + 1

            v = self.fields[attr]
            if isinstance(v, str):
                v = vstr
            elif isinstance(v, int):
                v = int(vstr)
            elif isinstance(v, float):
                v = float(vstr)
            else:
                raise ValueError('Unknown type {} for {}'.format(type(v), attr))
            self.fields[attr] = v

    def pack(self, buf):
        buf.empty()
        self.GRH.pack(buf)
        for attr, v in self.fields.items():
            fieldsize = FIELD_SIZES[attr]
            line = ' ' * 30 + '= ' + ' ' * (fieldsize - 33)
            attr = string.strip(attr)
            la = len(attr)
            line = string.upper(attr) + line[la:]

            if isinstance(v, str):
                vstr = v
            elif isinstance(v, (int, float)):
                vstr = str(v)
            else:
                logging.error('write: WARNING {} = {}'.format(attr, None))
                vstr = ''
                raise ValueError

            vstr = string.strip(vstr)
            lv = len(vstr)
            line = line[0:fieldsize - lv - 1] + vstr + '\n'

            self.buf.s = self.buf.s + line
            self.buf.k = self.buf.k + len(line)

            report = 0
            if self.buf.k > self.GRH.size:
                report = 1

            if report:
                print('WR BUF.K:', self.buf.k, self.GRH.size)
                print(len(self.buf.s))



    def complete(self):
        """
        """
        self.GeneralTime_SensingStart = GeneralTimeClass(
            self.fields['SENSING_START']
        )
        self.GeneralTime_SensingStart.get()
        self.GeneralTime_SensingEnd = GeneralTimeClass(
            self.fields['SENSING_END']
        )
        self.GeneralTime_SensingEnd.get()
