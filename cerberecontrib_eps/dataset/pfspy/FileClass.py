"""
File class for EPS format (refactoring of H.Bonekamp code)
"""
import logging

from .GRH import GRH
from .MPHR import MPHR
from .IPR import IPR
from .SPHR import SPHR

from .PFSRecord import PFSRecord, RecordClassDict
from .MDR import MDR
from .MDREngineering import MDREngineering
from .MDRVerification import MDRVerification
from .GEADR import GEADR
from .GIADR import GIADR
from .GIADREngineering import GIADREngineering
from .GIADRQuality import GIADRQuality
from .GIADRScaleFactors import GIADRScaleFactors
from .VEADR import VEADR
from .VIADR import VIADR
from .VIADREngineering import VIADREngineering


class FileClass:
    """File class for EPS format
    """
    def __init__(self,
                 filename: str,
                 template: 'RecordTemplate',
                 ):
        self.filename = filename
        self.template = template
        self.counter = 0
        self.fd = None

    def open(self):
        self.fd = open(self.filename, mode='rb')

    def close(self):
        self.fd.close()

    def read(self, specific=1):
        grh = GRH()
        grh.read(self.fd)

        logging.debug("Read {}".format(grh))

        if grh.record_class == RecordClassDict['MPHR']:
            #rec = MphrClass(self.template)
            rec = MPHR()
            rec.read(self.fd, grh)
            rec.complete()
        elif grh.record_class == RecordClassDict['SPHR']:
            rec = self.read_sphr(grh)
        elif grh.record_class == RecordClassDict['IPR']:
            rec = IPR()
            rec.read(self.fd, grh)
        elif grh.record_class == RecordClassDict['GEADR']:
            rec = self.read_geadr(grh)
        elif grh.record_class == RecordClassDict['GIADR']:
            rec = self.read_giadr(grh)
        elif grh.record_class == RecordClassDict['VEADR']:
            rec = self.read_veadr(grh)
        elif grh.record_class == RecordClassDict['VIADR']:
            rec = self.read_viadr(grh)
        elif grh.record_class == RecordClassDict['MDR']:
            rec = self.read_mdr(grh, specific=specific)
        else:
            logging.debug('########## STRANGE RECORD #########')
            rec = PFSRecord()
            rec.read(self.fd, grh)

        return rec

    def read_sphr(self, grh):
        rec = SPHR()
        rec.read(self.fd, grh)
        return rec

    def read_geadr(self, grh):
        rec = GEADR()
        rec.read(self.fd, grh)
        logging.debug('STRANGE File L1 has NO GEADR')
        return rec

    def read_veadr(self, grh):
        rec = VEADR()
        rec.read(self.fd, grh)
        logging.debug('STRANGE File L1 has NO VEADR')
        return rec

    def read_giadr(self, grh):
        """
        GIADR
        """
        if grh.record_subclass == 0:
            rec = GIADRQuality()
            rec.read(self.fd, grh)
            
        elif grh.record_subclass == 1:
            rec=GIADRScaleFactors()
            rec.read(self.fd, grh)

        elif grh.record_subclass == 2:
            rec=GIADREngineering()
            rec.read(self.fd, grh)
            
        else:
            rec = GIADR()
            rec.read(self.fd, grh)
            logging.debug('STRANGE GIADR')
        
        return rec

    def read_viadr(self, grh):
        """
        VIADR
        """
        if grh.record_subclass == 0:
            rec = VIADREngineering()
            rec.read(self.fd, grh)
            
        else:
            rec = VIADR()
            rec.read(self.fd, grh)
            logging.debug('STRANGE VIADR')
        
        return rec

    def read_mdr(self, grh, specific=1):
        """
        MDR 
        """
#         if grh.record_subclass == 0:
#             rec=MdrL1aClass(specific=specific)
#             rec.read(self.fd,grh)
#             
#         elif grh.record_subclass == 1:
#             rec=MdrL1bClass(specific=specific)
#             rec.read(self.fd,grh)
#             
#         elif grh.record_subclass == 2:
#             rec=MdrL1cClass(specific=specific)
#             rec.read(self.fd,grh)

        if grh.record_subclass == 3:
            rec = MDREngineering(self.template)
            rec.read(self.fd, grh)

        elif grh.record_subclass == 4:
            rec = MDRVerification()
            rec.read(self.fd, grh)
            
        elif grh.record_subclass in [0, 1, 2]:
            rec = MDR(self.template)
            rec.read(self.fd, grh)
        else:
            raise TypeError(
                "Unknow record type {}".format(grh.record_subclass)
                )

        return rec
