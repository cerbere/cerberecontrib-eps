"""
PFS GIADREngineering (refactoring of H.Bonekamp code)
"""
from .GIADR import GIADR


class GIADREngineering(GIADR):
    """
    """
    def __init__(self, *args, **kwargs):
        super(GIADREngineering, self).__init__(*args, **kwargs)
        self.record_subclass = 2
