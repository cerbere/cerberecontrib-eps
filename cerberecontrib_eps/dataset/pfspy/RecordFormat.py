"""
EPS PFS FIELD CLASS

GPFS 6.6 TABLE 17
"""
import numpy as np

from .BinaryBuffer import BinaryBuffer
from .PfsGlobals import GpfsFormatDict


class RecordFormat(object):
    """
    """
    nbytes = 30

    def __init__(self, descriptor, value=0, dimsizes=[1, 1, 1, 1]):
        self.descriptor = descriptor
        self.dimsizes = dimsizes
        self.value = [value] * np.prod(self.dimsizes)

    def create_buffer(self):
        return BinaryBuffer(s=np.full((self.nbytes,), b'1', dtype=bytes))

    def pack(self, buf):
        res = self.rescale(self.value)
        dtype = np.dtype(GpfsFormatDict[self.descriptor.rtype])
        length = np.prod(self.dimsizes)
        fmtall = ', '.join([dtype.str] * length)
        buf.pack(fmtall, res)

    def unpack(self, buf):
        length = np.prod(self.dimsizes)
        dtype = np.dtype(GpfsFormatDict[self.descriptor.rtype])
        res = buf.unpack(dtype, length)
        self.value = self.scale(res)

    def scale(self, arr):
        """
        scaling for floats  if required
        """
        scaling = self.descriptor.scaling
        if scaling is not None:
            if isinstance(arr, (np.ndarray, list, tuple)):
                res = np.ma.array(arr).astype(float) * scaling
            else:
                res = float(arr) * scaling
        else:
            res = arr
        return res

    def rescale(self, arr):
        """
        rescaling for floats  if required
        """
        scaling = self.descriptor.scaling
        if scaling is not None:
            if isinstance(arr, (np.ndarray, list, tuple)):
                res = np.ma.array(arr).astype(float) / scaling
            else:
                res = int(arr * scaling)
        else:
            res = arr
        return res

    def __str__(self):
        res = self.fieldname + '\n'
        res += '   description: {}'.format(self.fieldname)
        res += '   scale factor: {}'.format(self.scaling)
        res += '   unit: {}'.format(self.unit)
        res += '   type: {}'.format(self.rtype)

        for k in range(0, self.dim3):
            kpp = k * self.dim1 * self.dim2
            for j in range(0, self.dim2):
                jpp = j * self.dim1 + kpp
                for i in range(0, self.dim1):
                    ipp = i + jpp
                    res += '    {} {} {} {} : {}\n'.format(
                        k, j, i, ipp, self.value[ipp]
                    )
        return res
