"""
EPS PFS Long Generalised Time Class
"""
from GeneralTimeClass import GeneralTimeClass


class LongGeneralTimeClass(GeneralTimeClass):
    """
    """
    def __init__(self, s='00000000000000000Z'):
        GeneralTimeClass.__init__(self, s=s, L=18)
        self.long = 1
