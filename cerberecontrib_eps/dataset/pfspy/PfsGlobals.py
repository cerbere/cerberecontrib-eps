"""
PFS Globals (refactoring of H.Bonekamp code)
"""
import struct

from numpy import dtype


# Record Class Dictionary
RecordClassList = [
    'RESERVED',  # 0
    'MPHR',  # 1
    'SPHR',  # 2
    'IPR',  # 3
    'GEADR',  # 4
    'GIADR',  # 4
    'VEADR',  # 6
    'VIADR',  # 7
    'MDR']  # 8

RecordClassDict = {
    'RESERVED': 0,
    'MPHR': 1,
    'SPHR': 2,
    'IPR': 3,
    'GEADR': 4,
    'GIADR': 5,
    'VEADR': 6,
    'VIADR': 7,
    'MDR': 8
}

# ----------------
# GPFS 6.6 TABLE  13
# ----------------

InstrumentGroup = ['GENERIC', 'AMSU-A', 'ASCAT' 'ATOVS', 'AVHRR/3', 'GOME',
                   'GRAS',
                   'HIRS/4', 'IASI', 'MHS', 'SEM', 'ADCS', 'SBUV', 'DUMMY']

InstrumentGroupNumber = {'GENERIC': 0, 'AMSU-A': 1, 'ASCAT': 2, 'ATOVS': 3,
                         'AVHRR/3': 4,
                         'GOME': 5, 'GRAS': 6, 'HIRS/4': 7, 'IASI': 8, 'MHS': 9,
                         'SEM': 10, 'ADCS': 11, 'SBUV': 12, 'DUMMY': 13}

# =============
# endian
# =============

num = struct.pack("@I", 0x01020304)
big_endian = (num == "\x01\x02\x03\04")
little_endian = (num == "\x04\x03\x02\01")

endian = big_endian * '>' + little_endian * '<'

GpfsEndian = '>'


GpfsFormatDict = {
    'bitst8': dtype(('a', 1)),
    'bitst16': dtype('>u2'),  # dtype(('a', 2)),
    'bitst32': dtype(('a', 4)),
    'bitst48': dtype(('a', 6)),
    'bitst64': dtype(('a', 8)),
    'bitst256': dtype(('a', 32)),
    'byte': dtype('i1'),
    'u-byte': dtype('u1'),
    'enumerated': dtype('i1'),
    'boolean': dtype('i1'),
    'integer2': dtype('>i2'),
    'u-integer2': dtype('>u2'),
    'integer4': dtype('>i4'),
    'u-integer4': dtype('>u4'),
    'v-integer4': dtype('i1, >i4'),
    'integer8': dtype('>i8'),
    'u-integer8': dtype('>u8'),
    'REC_HEAD': dtype('(4)S, >u4, >u2, >u4, >u2, >u4'),
    'short cds time': dtype('>u2, >u4'),
    'long cds time': dtype('>u2, >u4, u2'),
    'general time': dtype(('a', 15)),
    'long general time': dtype(('a', 18)),
    'char': dtype('S1'),
}


GpfsFormatSizeDict = {}
for key in GpfsFormatDict.keys():
    GpfsFormatSizeDict[key] = dtype(GpfsFormatDict[key]).itemsize
