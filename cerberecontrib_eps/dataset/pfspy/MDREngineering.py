"""
PFS MDREngineering (refactoring of H.Bonekamp code)
"""
from .MDR import MDR


class MDREngineering(MDR):
    """ 
    """
    def __init__(self, template, *args, **kwargs):
        super(MDREngineering, self).__init__(template, *args, **kwargs)
        self.record_subclass = 3
