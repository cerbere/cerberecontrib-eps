"""
EPS PFS Generic Record Header Class
"""

import numpy as np

from .BinaryBuffer import BinaryBuffer
from .CDSTime import METOP_EPOCH
from .PfsGlobals import RecordClassList
from .RecordTemplate import Descriptor
from .ShortCDSTime import ShortCDSTime


class GRH(object):
    """Generic Record Header (GRH)"""
    nbytes = 20
    description = 'GENERIC RECORD HEADER'

    def __init__(self):
        """
        Generic  Record Header
        """
        super(GRH, self).__init__()

        self.record_class = 1
        self.instrument_group = 0
        self.record_subclass = 0
        self.record_subclass_version = 2
        self.size = 3307

        self.record_start_time = METOP_EPOCH
        self.record_stop_time = METOP_EPOCH

    def unpack(self, buf):
        # BBBBIHIHI
        buf.k = 0
        self.record_class = buf.unpack('>B')[0]
        self.instrument_group = buf.unpack('>B')[0]
        self.record_subclass = buf.unpack('>B')[0]
        self.record_subclass_version = buf.unpack('>B')[0]
        self.size = buf.unpack('>i4')[0]

        tmp = ShortCDSTime(Descriptor(rtype='cds short time'))
        tmp.unpack(buf)
        self.record_start_time = tmp.value

        tmp.unpack(buf)
        self.record_stop_time = tmp.value

    def pack(self, buf):
        buf.k = 0
        buf.pack('>B', self.record_class)
        buf.pack('>B', self.instrument_group)
        buf.pack('>B', self.record_subclass)
        buf.pack('>B', self.record_subclass_version)
        buf.pack('>I', self.size)

        tmp = ShortCDSTime(self.record_start_time)
        tmp.pack(buf)

        tmp = ShortCDSTime(self.record_stop_time)
        tmp.pack(buf)

    def create_buffer(self):
        return BinaryBuffer(s=np.zeros(shape=(self.nbytes,), dtype=bytes))

    def read(self, fd):
        """
        read values
        """
        buf = self.create_buffer()
        buf.read(fd, self.nbytes)
        self.unpack(buf)

    def write(self, fd):
        """
        write GRH values
        """
        buf = BinaryBuffer(s=np.zeros(shape=(self.nbytes,), dtype=bytes))
        self.pack(buf)
        buf.k = 0
        buf.write(fd)
     
    def init_values(self):
        self.record_class = 1
        self.instrument_group = 0
        self.record_subclass = 0
        self.record_subclass_version = 2
        self.size = 3307

    def __str__(self):
        RecordClass = RecordClassList[self.record_class]
        res = '  {}: {}\n'.format(RecordClass, self.__class__.__name__)
        res += '     instrument_group: {}\n'.format(self.instrument_group)
        res += '     record_subclass: {}\n'.format(self.record_subclass)
        res += '     record_subclass_version: {}\n'.format(
            self.record_subclass_version)
        res += '     size: {}\n'.format(self.size)
        res += '     start time: {}'.format(self.record_start_time)
        res += '     stop time: {}'.format(self.record_stop_time)
        return res
