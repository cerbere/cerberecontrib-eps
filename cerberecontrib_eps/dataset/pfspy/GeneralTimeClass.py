#!/usr/bin/env python
#
###################################
#    Hans Bonekamp      EUMETSAT2005
###################################


"""
  EPS PFS
  
  Generalised  Time Class

  Long Generalised Time Class

"""

from  time import *

import string

WeekDays=['Monday',
          'Tuesday',
          'Wednesday',
          'Thursday',
          'Friday',
          'Saturday',
          'Sunday']

#=================================================================
class GeneralTimeClass:
    #  #[ 
    """

    """
    
    #-------------------------------------------------
    def __init__(self,s='00000000000000Z',L=15):
        """
        """
        self.L = L # length
        self.s = s
        self.get()
        self.TestFormat()
        self.long = 0
        self.prefix = 'Generalised Time:'
        self.field  = 'Generalised_Time:'

    #-------------------------------------------------
    def SetTuple(self):
        """
         Set Python Tuple 
        """
        self.Tuple = (self.Year,self.Month,self.Day,self.Hour,  
                   self.Minute,
                   self.Second,-1,-1,-1)
        secs= mktime(self.Tuple)
        self.Tuple= gmtime(secs)
        self.WeekDay  =  WeekDays[self.Tuple[6]]
        self.JulianDay = self.Tuple[7]



    #-------------------------------------------------
    def TestFormat(self):
        if len(self.s) != self.L:
            print('GeneralTimeClass: length string incorrect')
        if self.s[self.L-1] != 'Z':
            print('GeneralTimeClass:  missing ZULU')

    #-------------------------------------------------
    def get(self):
        """
        str-> values
        """
        self.Year = int(self.s[ 0: 4])
        self.Month = int(self.s[ 4: 6])
        self.Day = int(self.s[ 6: 8])
        self.Hour = int(self.s[ 8:10])
        self.Minute = int(self.s[10:12])
        self.Second = int(self.s[12:14])
        if self.L==18:
            self.MilliSecond = int(self.s[14:17])
        else:
            self.MilliSecond = 0

        self.SetTuple()

    #-------------------------------------------------
    def put(self):
        """
        values -> string
        """
        self.s = ''
        self.s = self.s + str(int(self.Year +10000  ))[1:]
        self.s = self.s + str(int(self.Month+100  ))[1:]
        self.s = self.s + str(int(self.Day+100    ))[1:]
        self.s = self.s + str(int(self.Hour+100   ))[1:]
        self.s = self.s + str(int(self.Minute+100 ))[1:]
        self.s = self.s + str(int(self.Second+100 ))[1:]
        if self.L==18:
            self.s = self.s + str(int(self.MilliSecond +1000 ))[1:]
        self.s = self.s + 'Z'  # ZULU 



    #-------------------------------------------------
    def DisplayValues(self, prefix='' ):
        """
        """
        if prefix:
            self.prefix = prefix

        print("%s" % self.prefix)
        print("%6d" % self.Year)
        print("%6d" % self.Month)
        print("%6d" % self.Day)
        print("%6d" % self.Hour)
        print("%6d" % self.Minute)
        print("%6d" % self.Second)
        print("%6d" % self.MilliSecond)
        print("%6d" % self.JulianDay)
        print("%10s" % self.WeekDay)

    def display(self, prefix=''):
        """
        display class entries
        """
        self.get()
	
        if prefix:
            self.prefix = prefix

        print(self.prefix, self.field,"Year : ",self.Year)
        print(self.prefix, self.field,"Month : ",self.Month)
        print(self.prefix, self.field,"Day : ",self.Day)
        print(self.prefix, self.field,"Hour : ",self.Hour)
        print(self.prefix, self.field,"Minute          : ",self.Minute)
        print(self.prefix, self.field,"Second         : ",self.Second)
        print(self.prefix, self.field,"MilliSecond    : ",self.MilliSecond)
        print(self.prefix, self.field,"JulianDay       : ",self.JulianDay)
        print(self.prefix, self.field,"WeekDay         : ",self.WeekDay)
        
        if self.long:
            print(self.prefix, self.field,"MicroSecond  : ",self.MicroSecond)

    def DisplayString(self, prefix='' ):
        print(prefix,'Time ',  self.s)

    def setvalues(self):
        self.Year   = 2006
        self.Month  = 10
        self.Day    = 27
        self.Hour   = 20
        self.Minute = 42
        self.Second = 2
        self.MilliSecond = 345
        self.put()
        self.get()

    def test(self):
        self.setvalues()
        self.DisplayString()
        self.DisplayValues()
        self.display()


if __name__ == '__main__':   #EXECUTABLE
    instance = GeneralTimeClass()
    instance.test()


