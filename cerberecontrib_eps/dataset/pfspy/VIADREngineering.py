"""
PFS VIADREngineering (refactoring of H.Bonekamp code)
"""
from .VIADR import VIADR


class VIADREngineering(VIADR):
    """ 
    """
    def __init__(self, **kwargs):
        super(VIADREngineering, self).__init__(self, **kwargs)
        self.record_subclass = 0
