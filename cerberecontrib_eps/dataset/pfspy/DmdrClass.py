#!/usr/bin/env python
#

# ---------------------------------------------
#  CURR Code Owner: Hans Bonekamp
#  

"""
 PFS

 Hans Bonekamp 

"""

import string
import tempfile
import os

from MdrClass import MdrClass 

from BinaryRecordClass import BinaryRecordClass
from BinaryRecordClass import Create_BinaryRecordClassList


#
#=================================================================

DefinitionLines="""
SPARE_FLAG ;'Default Value'; ;     ;1;1;1;enumerated
"""

BinaryRecordClassList= Create_BinaryRecordClassList(DefinitionLines)


# BinaryRecordClassList[0].display_def()

#=================================================================

class DmdrClass(MdrClass):
    """
        
    """
    #  #[
    version ='0.1'
    #-----------------------------------------
    def __init__(self, **params):
        """
        """
        MdrClass.__init__(self)
        self.acronym='DUMMY MDR'
        self.instrument_group = 13
        self.record_subclass = 1
        self.prefix='DUMMY MDR: '
        self.AddFieldList(BinaryRecordClassList)
        self.DisplayList  =  ['SPARE_FLAG']        

    #  #] 
#=================================================================
#




#=================================================================
#  #[ TEST SCRIPT
#
if __name__ == '__main__':   #EXECUTABLE
    instance =  DmdrClass()
    instance.test()
    

else:
    pass  # importable as a module


#  #]
#=================================================================







































