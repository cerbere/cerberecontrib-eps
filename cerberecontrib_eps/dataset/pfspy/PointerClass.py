"""
EPS GPFS IPR: Internal Pointer Record
"""
import numpy as np

from .BinaryBuffer import BinaryBuffer


class PointerClass:
    def __init__(self):
        self.record_class = 0
        self.instrument_group = 0
        self.record_subclass = 0
        self.record_offset = 0
        self.field = 'PTR: '

    def __str__(self):
        res = 'PTR\n'
        res += '     instrument_group: {}\n'.format(self.instrument_group)
        res += '     record_class: {}\n'.format(self.record_class)
        res += '     record_subclass: {}\n'.format(self.record_subclass)
        res += '     size: {}\n'.format(self.record_offset)
        return res

    def as_buf(self):
        buf = BinaryBuffer(np.full((10,), b' ', dtype=bytes))
        self.pack(buf)
        return buf

    def unpack(self, buf):
        self.record_class = buf.unpack('>B')[0]
        self.instrument_group = buf.unpack('>B')[0]
        self.record_subclass = buf.unpack('>B')[0]
        self.record_offset = buf.unpack('>I')[0]

    def pack(self, buf):
        buf.pack('>B', self.record_class)
        buf.pack('>B', self.instrument_group)
        buf.pack('>B', self.record_subclass)
        buf.pack('>I', self.record_offset)

    def init_values(self):
        self.record_class = 0
        self.instrument_group = 0
        self.record_subclass = 0
        self.record_offset = 0

