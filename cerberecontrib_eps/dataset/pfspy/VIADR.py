"""
PFS VIADR (refactoring of H.Bonekamp code)
"""
from .PFSRecord import PFSRecord


class VIADR(PFSRecord):
    """
    """
    def __init__(self, *args, **kwargs):
        super(VIADR, self).__init__(*args, **kwargs)
        self.record_class = 7

