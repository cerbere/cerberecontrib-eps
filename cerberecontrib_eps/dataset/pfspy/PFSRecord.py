"""
PFS (refactoring of H.Bonekamp code)
"""
import logging
import string

import numpy as np

from .GRH import GRH
from .BinaryBuffer import BinaryBuffer
from .PfsGlobals import RecordClassDict


def fromline(line):
    ieq = string.index(line, '=')
    name = string.lstrip(line[0:ieq])
    value = string.strip(line[ieq+1:])
    return name, value,
    

class PFSRecord(object):
    """
    """
    def __init__(self, display_list=[], counter=0):

        self.record_class = None
        self.GRH = GRH()
        self.GRH.record_class = self.record_class

        self.fields = {}
        self.display_list = display_list
        self.counter = counter 

    def add_field(self, fieldname: str, br: 'BinaryRecord'):
        self.fields[fieldname] = br

    def add_fields(self, fields):
        for fieldname, br in fields.items():
            self.add_field(fieldname, br)

    def read(self, fd, grh=None):
        buf = BinaryBuffer(s=np.zeros(shape=(100,), dtype=bytes))

        # read or assign header
        if grh is not None:
            self.GRH = grh
        else:
            buf.read(fd, self.GRH.nbytes)
            self.GRH.unpack(buf)

        # skip header and read data
        buf.k = self.GRH.nbytes
        buf.read(fd, self.GRH.size - self.GRH.nbytes)

        buflen = len(buf.s)
        if self.GRH.size < buflen: 
            logging.warning("LEN  TOO BIG ", self.GRH.size, buflen)
        if self.GRH.size > buflen:
            logging.warning("LEN  TOO SMALL ", self.GRH.size, buflen)

        # unpack buffer
        buf.k = 0
        self.unpack(buf)

    def write(self, fd):
        """
        write values
        """
        buf = BinaryBuffer(s=np.zeros(shape=(100,), dtype=bytes))
        self.pack(buf)
        self.write_buf(fd)

    def write_buf(self, fd, buf):
        """
        write values
        """ 
        buf.write(fd)

    def unpack(self, buf):
        """
        """
        buf.k = self.GRH.nbytes
        for fieldname, br in self.fields.items():
            br.unpack(buf)
            self.fields[fieldname] = br

    def pack(self, buf):
        """
        """
        buf.empty()
        self.GRH.pack(buf)
        for fieldname, br in self.fields.items():
            br.pack(buf)

    def display(self, prefix=''):
        if self.verbose:
            first=1
            sf3 =string.lower(self.acronym)
            if prefix:
                self.prefix=prefix
            
            for field2 in self.field_list:
                for field in self.display_list:

                    #print 'field:',field
                    sf1 = string.lower(field)
                    sf2 = string.lower(field2)
                    if  (sf1==sf2) or (sf1==sf3):
                        if first:    
                            print('RECORD:', self.counter)
                            print(self.GRH)
                            first=0
                        fld=getattr(self,field2)

                        if type(fld)==type(''):
                            print(self.prefix, '', field2, ': ', fld)
                        elif type(fld)==type(3):
                            print(self.prefix, '', field2, ': ', fld)
                        else:
                            fld.display()
                            
    def is_mdr(self):
        return self.record_class == RecordClassDict['MDR']

    def is_viadr(self):
        return self.record_class == RecordClassDict['VIADR']

    def is_giadr(self):
        return self.record_class == RecordClassDict['GIADR']
