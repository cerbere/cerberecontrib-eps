"""
Record structure for any EPS products
"""
import os
import configparser
from collections import OrderedDict
import ast

import numpy


MDR_PREFIX = 'MDR:variable:'


class Descriptor(object):
    """Descriptor of a field"""
    def __init__(self,
                 rtype,
                 name=None,
                 dtype=numpy.float32,
                 description=None,
                 unit=None,
                 dims=None,
                 broadcast_dims=None,
                 scaling=None,
                 fill_value=None):
        self.description = description
        self.unit = unit
        self.dims = dims
        self.broadcast_dims = broadcast_dims
        self.scaling = scaling
        self.rtype = rtype
        self.dtype = dtype
        self.eps_name = name
        self.fill_value = fill_value


class RecordTemplate(object):
    """Base class describing the structure of EPS files for any
    product. the description is loaded from a template file.
    """

    def __init__(self, template):
        """
        """
        template_path = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'templates',
            template
        )
        if not os.path.exists(template_path):
            raise ValueError(
                "Configuration file is not existing : {}".format(template_path)
            )
        config = configparser.RawConfigParser()
        config.read(template_path)

        # read dimensions
        self.dims = {}
        self.main_dim = None
        for dim in config.options('dimensions'):
            dimval = config.getint('dimensions', dim)
            if dimval == -1:
                self.main_dim = dim
            else:
                self.dims[dim] = config.getint('dimensions', dim)
        if self.main_dim is None:
            raise ValueError('The dimension along MDR records must be defined')

        # coordinates variables
        self.coordinates = {
            'lat': config.get('coordinates', 'lat'),
            'lon': config.get('coordinates', 'lon'),
            'time': config.get('coordinates', 'time')
        }

        # read MDR variables
        self.MDR = OrderedDict([])
        for sect in config.sections():
            if sect.startswith(MDR_PREFIX):
                variable = sect.replace(MDR_PREFIX, '')
                self.MDR[variable] = Descriptor(config.get(sect, 'type'))

                if config.has_option(sect, 'description'):
                    self.MDR[variable].description = config.get(
                        sect, 'description'
                    )
                if config.has_option(sect, 'units'):
                    self.MDR[variable].unit = config.get(
                        sect, 'units'
                    )
                if config.has_option(sect, 'dtype'):
                    self.MDR[variable].dtype = eval(
                        config.get(sect, 'dtype').strip()
                    )
                self.MDR[variable].dims = []
                if config.has_option(sect, 'eps_dims'):
                    self.MDR[variable].dims = [
                        _.strip() for _ in config.get(
                            sect, 'eps_dims'
                        ).split(',')
                    ]
                self.MDR[variable].broadcast_dims = self.MDR[variable].dims
                if config.has_option(sect, 'broadcast_dims'):
                    self.MDR[variable].broadcast_dims = [
                        _.strip() for _ in config.get(
                            sect, 'broadcast_dims'
                        ).split(',')
                    ]
                if config.has_option(sect, 'scaling'):
                    self.MDR[variable].scaling = ast.literal_eval(
                        config.get(sect, 'scaling').strip()
                    )

                if config.has_option(sect, 'fill_value'):
                    self.MDR[variable].fill_value = ast.literal_eval(
                        config.get(sect, 'fill_value').strip()
                    )
                elif self.MDR[variable].fill_value is None:
                    if self.MDR[variable].dtype == numpy.datetime64:
                        self.MDR[variable].fill_value = numpy.datetime64('NaT')
                    elif self.MDR[variable].dtype != numpy.dtype(numpy.bool):
                        def_fill_value = numpy.ma.maximum_fill_value(
                            numpy.dtype(self.MDR[variable].dtype)
                        )
                        self.MDR[variable].fill_value = def_fill_value

                self.MDR[variable].eps_name = variable
                if config.has_option(sect, 'eps_name'):
                    self.MDR[variable].eps_name = config.get(
                        sect, 'eps_name'
                    ).strip()
