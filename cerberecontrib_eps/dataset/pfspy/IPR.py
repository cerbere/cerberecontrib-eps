"""
PFS (refactoring of H.Bonekamp code)
"""
from .PFSRecord import PFSRecord, RecordClassDict
from .PointerClass import PointerClass


class IPR(PFSRecord):
    """
    IPR class
    """
    def __init__(self):
        super(IPR, self).__init__()
        self.record_class = 3
        self.target = PointerClass()
        self.init_values()

    def unpack(self, buf):
        buf.k = self.GRH.nbytes
        self.target.unpack(buf)

    def pack(self, buf):
        buf.empty()
        self.GRH.pack(buf)
        self.target.pack(buf)

    def init_values(self):
        self.GRH.init_values()
        self.target.init_values()

