"""
PFS GiadrScaleFactors (refactoring of H.Bonekamp code)
"""
from .GIADR import GIADR


class GIADRScaleFactors(GIADR):
    """
    """
    def __init__(self, **kwargs):
        super(GIADRScaleFactors, self).__init__(**kwargs)
        self.record_subclass = 1


