"""
EPS PFS FIELD CLASS

GPFS 6.6 TABLE 17
"""
from .RecordFormat import RecordFormat


class BinaryRecord(RecordFormat):
    """
    EPS PFS FIELD CLASS
    """
    nbytes = 20



