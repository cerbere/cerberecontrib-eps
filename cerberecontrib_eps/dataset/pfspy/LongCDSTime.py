"""
EPS PFS Long Cds Time Class
"""
import datetime

import numpy as np

from .CDSTime import CDSTime, METOP_EPOCH


class LongCDSTime(CDSTime):

    def unpack(self, buf):
        # 'HIH',
        epoch = buf.unpack('>H')[0]
        millisec = buf.unpack('>I')[0]
        microsec = buf.unpack('>H')[0]
        self.value = np.datetime64(
            METOP_EPOCH + datetime.timedelta(
                days=int(epoch),
                milliseconds=int(millisec),
                microseconds=int(microsec)
            )
        )

    def pack(self, buf):
        buf.pack('>H', (self.value - METOP_EPOCH).days)
        buf.pack('>I', (self.value - METOP_EPOCH).microseconds // 1000)
        buf.pack('>H', (self.value - METOP_EPOCH).microseconds % 1000)

