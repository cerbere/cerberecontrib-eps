"""
.. module::cerbere.dataset.pfspy

PFS
(refactoring of H.Bonekamp code)

:copyright: Copyright 2013 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from .PFSRecord import PFSRecord
from .BinaryRecord import BinaryRecord
from .ShortCDSTime import ShortCDSTime
from .LongCDSTime import LongCDSTime


class MDR(PFSRecord):
    """
    """
    def __init__(self, template, **kwargs):
        super(MDR, self).__init__('MDR', **kwargs)
        self.record_class = 8
        self.template = template

        # build record structure from record description template        
        for field in template.MDR:
            dimsizes = [1, 1, 1, 1]
            for i, dim in enumerate(template.MDR[field].dims):
                dimsizes[i] = template.dims[dim]
            if template.MDR[field].rtype == 'short cds time':
                mdrclass = ShortCDSTime
            elif template.MDR[field].rtype == 'long cds time':
                mdrclass = LongCDSTime
            else:
                mdrclass = BinaryRecord

            self.add_field(
                    field,
                    mdrclass(
                        descriptor=template.MDR[field],
                        dimsizes=dimsizes
                    )
                )
