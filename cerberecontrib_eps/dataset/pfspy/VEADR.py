"""
PFS VEADR (refactoring of H.Bonekamp code)
"""
import numpy as np

from .BinaryRecord import BinaryRecord
from .PFSRecord import PFSRecord
from .RecordTemplate import Descriptor


class VEADR(PFSRecord):
    """
    """
    def __init__(self, **params):
        super(VEADR, self).__init__(
            display_list=['AUX_DATA_POINTER']
        )
        self.record_class = 6
        self.add_field(
            'AUX_DATA_POINTER',
            BinaryRecord(
                Descriptor(
                    rtype='char',
                    description='Unique pointer to auxiliary dataset')
            )
        )

    def unpack(self, buf):
        super(VEADR, self).unpack(buf)
        self.fields['AUX_DATA_POINTER'].value = buf.s[20:100].tostring().decode('utf-8')

    def pack(self, buf):
        self.buf.empty()
        self.GRH.pack()
        self.buf.s[20:100] = np.frombuffer(self.fields['AUX_DATA_POINTER'].value)
