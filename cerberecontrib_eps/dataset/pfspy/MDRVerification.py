"""
PFS (refactoring of H.Bonekamp code)
"""
from .MDR import MDR
from .BinaryRecord import BinaryRecord


class MDRVerification(MDR):
    """
    """
    def __init__(self, template, *args, **kwargs):
        MDR.__init__(self, template, *args, **kwargs)
        self.record_subclass = 4
        self.add_field(
            'SIZE_OF_VERIFICATION_DATA',
            BinaryRecord(
                description='Size of the verification data and auxiliary data to follow',
                rtype='u-integer4',
                scale_factor=0,
                unit='bytes'
            )
        )
        self.add_field(
            'VERIFICATION_DATA',
            BinaryRecord(
                description='Verification and auxiliary data ordered as per Section 7.3',
                rtype='u-byte',
                dimensions=[self.N, 1, 1, 1],
                unit='bytes'
            )
        )
