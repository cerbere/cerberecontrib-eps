"""
  EPS PFS
  GPFS 6.6
"""
import datetime

from .BinaryRecord import BinaryRecord


METOP_EPOCH = datetime.datetime(2000, 1, 1, 0, 0, 0, 0)

DEFAULT_UNITS = 'seconds since {}'.format(
    METOP_EPOCH.strftime("%Y-%m-%d %H:%M:%S")
    )


class CDSTime(BinaryRecord):
    """Base class for both ShortCDSTime Class and LongCdsTime Class"""
    def __init__(self,
                 descriptor,
                 value=METOP_EPOCH,
                 **kwargs):
        super(CDSTime, self).__init__(descriptor, value=value, **kwargs)
        if descriptor.unit is None:
            self.descriptor.unit = DEFAULT_UNITS
