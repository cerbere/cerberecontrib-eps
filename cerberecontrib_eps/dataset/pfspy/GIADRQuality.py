"""
PFS GIADRQuality (refactoring of H.Bonekamp code)
"""
from .GIADR import GIADR


class GIADRQuality(GIADR):
    """ 
    """
    def __init__(self, *args, **kwargs):
        super(GIADRQuality, self).__init__(*args, **kwargs)
        self.record_subclass = 2
