"""
Bin
"""
import numpy as np


class BinaryBuffer:
    """
    The purpose of the BinBufClass is
    to provide byte stream handling in a  packed string

    S :the packed string
    k : offset in the packed string s 

    """
    def __init__(self, s=np.ma.zeros(shape=(20,), dtype=bytes)):
        """
        - create s
        - set  k = 0
        """
        self.s = s  # packed string
        self.k = 0  # offset
        
    def empty(self):
        """
        """
        self.s = np.empty(dtype=bytes)
        self.k = 0

    def __str__(self):
        prefix = 'BUF:\n'
        return prefix + '\n'.join([
            "  size : {}".format(len(self.s)),
            "  buf  : {}".format(self.s),
            "  k    : {}".format(self.k)
        ])

    def read(self, fd, m=0):
        """
        read m bytes from file descriptor fd into s
        shifts k  m places
        """
        if not m:
            self.k = 0
            m = len(self.s)
        
        self.s = np.concatenate(
            (self.s[0:self.k], np.frombuffer(fd.read(m), dtype='S1'))
        )
        self.k = self.k + m

    def read_all(self, fd):
        """
        read
        """
        self.s = fd.read()
        self.k = len(self.s)

    def write(self, fd, m=0):
        """
        write m bytes from s to file descriptor fd
        shifts k  m places
        """
        if not m:
            self.k = 0
            m = len(self.s) 
        fd.write(self.s[self.k:self.k+m])
        self.k = self.k + m

    def unpack(self, fmt, length=1):
        """
        unpack from last m bytes from into a value
        using  format fmt and struct module
        m = corresponding byte size of fmt
        returns value
        shifts k  m places
        (see  struct module)
        """
        format = np.dtype(fmt)
        m = format.itemsize * length
        self.k = self.k + m
        val = np.frombuffer(
            self.s[self.k-m: self.k], dtype=format, count=length
        )
        return val

    def pack(self, fmt, val):
        """
        packs value(s) val into buffer s
        using format fmt and struct module
        m = corresponding byte size of fmt
        shifts k  m places
        (see  struct module)
        """ 
        vl = val
        if isinstance(val, list):
            vl = tuple(val)
        elif not isinstance(val, tuple):
            vl = (val,)

        tutu = np.array(vl, dtype=np.dtype(fmt))
        s = np.frombuffer(tutu.tobytes(), dtype='S1')
        m = len(s)
        self.s = np.concatenate((self.s[0:self.k], s, self.s[self.k+m:]))
        self.k = self.k + m

