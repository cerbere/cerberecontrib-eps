#!/usr/bin/env python
#
###################################
#    Hans Bonekamp      EUMETSAT  2006
###################################


"""
  EPS PFS

  FIELD CLASS

  GPFS 6.6 paragraph  4.4.1

  Hans Bonekamp  

"""

import os
import sys
import string


from BinBufClass       import BinBufClass
from PfsGlobals        import GpfsFormatDict
from PfsGlobals        import GpfsFormatSizeDict
from PfsGlobals        import GpfsEndian
from ShortCdsTimeClass import ShortCdsTimeClass
from LongCdsTimeClass  import LongCdsTimeClass

from RecordFormatClass import RecordFormatClass

#=============
# LINE interpreter
#=============

def Create_AsciiRecordClassList(linesall):
    """
    line  interpreting funtion
    returns a list with attribute  specifications of a record
    to initiate the corresponding  record class

    """
    AsciiRecordClassList=[]
    lines=string.split(linesall,sep='\n')
    
    for line in lines:
        words= string.split(line,sep=';')
        if len(words)==8:
            # print words
            typ = words[7].strip()

            if typ=='short cds time':
                fc = ShortCdsTimeClass()
            elif typ=='long cds time':
                fc = LongCdsTimeClass()
            else:
                fc = AsciiRecordClass()

            fc.type = typ

            try:
                fc.sf  = int(words[2])
            except:
                fc.sf  = None

            fc.unit        = words[3].strip()
            fc.dim1        = int(words[4])
            fc.dim2        = int(words[5])
            fc.dim3        = int(words[6])  
            fc.fieldname   = words[0].strip()
            fc.description = words[1].strip()
            
            AsciiRecordClassList.append(fc)
            del fc
        else:
            pass
            #print 'WORDS:',words
    return AsciiRecordClassList



#=================================================================
class AsciiRecordClass(RecordFormatClass):
    #  #[ 
    """

    """
    version ='0.1'
    NrBytes = 20
    

    #-------------------------------------------------
    def __init__(self,buf=BinBufClass('1'*30),                 
                 fieldname='FLD',
                 description='field description',
                 sf         = None,
                 unit       = '',
                 encodechars ='', 
                 dim1       = 1,
                 dim2       = 1,
                 dim3       = 1,
                 type       = 'integer2',
                 previous   =  None,
                 next       = None,
                 offset    = 20,
 
                 ):
        """
        """
        RecordFormatClass.__init__(self,
                                   buf=buf,
                                   fieldname=fieldname,
                                   description=description,
                                   scale_factor= sf,
                                   unit       = unit,
                                   rtype= type,
                                   previous   =  previous,
                                   next       = next,
                                   offset    = offset
                                   )
        
        self.dim1               = dim1
        self.dim2               = dim2
        self.dim3               = dim3
        self.encodechars        = encodechars 
        self.set_sizes()
        

    #-------------------------------------------------
    def test(self):
        """
        Class test
        """
        k=self.buf.k
        self.display_def()
        self.pack()
        self.display()
        self.buf.k=k
        self.pack()
        self.display()
        self.buf.k=k
        print 'ok'

#  #]
#=================================================================


#=================================================================
#  #[ TEST SCRIPT
#
if __name__ == '__main__':   #EXECUTABLE
    instance = AsciiRecordClass()
    instance.test()


else:
    pass  # importable as a module


#  #]
#=================================================================


