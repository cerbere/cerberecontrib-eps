#!/usr/bin/env python
###################################
#    Hans Bonekamp      EUMETSAT2005
###################################
# 

"""
  EPS PFS
  GPFS 6.6


"""

import time
import math


from GeneralTimeClass import GeneralTimeClass




#----------------
# GPFS 6.6 TABLE  24
#----------------
SPACECRAFT_ID ={'M01':'METOP B',
                'M02':'METOP A',
                'MO3':'METOP C',
                'N15':'NOAA 15',
                'N16':'NOAA 16',
                'N17':'NOAA 17',
                'N18':'NOAA 18',
                'N19':'NOAA 19', 
                'xxx':'No Specfic'
                }


#----------------
# GPFS 6.6 TABLE  23
#----------------
PROCESSING_LEVEL ={'00':'Level 0',
                '01':'Level 1',
                '1A':'Level 1a',
                '1B':'Level 1b',
                '1C':'Level 1c',
                '02':'Level 2',
                '03':'Level 3',
                'xx':'No Specfic',
                }



#----------------
# GPFS 6.6 TABLE  25
#----------------
PROCESSING_CENTRE ={'DMIx':'GRAS SAF',
                 'DWDx':'Climate SAF',
                 'FMIx':'OZONE SAF',
                 'INMx':'NWC SAF',
                 'INPx':'LAND SAF',
                 'MFxx':'OSI SAF',
                 'UKMO':'NWP SAF',
                 'xxxx':'No Specfic',
                }


#----------------
# GPFS 6.6 TABLE  26
#----------------
PROCESSING_MODE ={'N':'Nominal',
                  'B':'Backlog',
                  'R':'Reprocessing',
                  'V':'Reprocessing',
                  }

#----------------
# GPFS 6.6 TABLE  27
#----------------
DISPOSITION_MODE ={'T':'Testing',
                  'O':'Operational',
                  'C':'Commissioning',
                  }

#----------------
# GPFS 6.6 TABLE  28
#----------------
RECEIVING_GROUND_STATION ={'SVL':'Svalbard',
                           'WAL':'Wallops',
                           'FBK':'Fairbanks',
                           'SOC':'Suitland',
                           'RUS':'Reference User Station',
                           }


#=================================================================


class ProductNameClass:
    #  #[ 
    """


    """

    #-------------------------------------------------
    def __init__(self,s='0'*67):
        """
        """
        self.s=s
        self.prefix= 'PRODOUCT NAME: '
        self.field= 'PRODOUCT NAME: '
        
        self.get()

    #-------------------------------------------------
    def get(self):
        """
                   1         2         3         4         5         6
         0123456789012345678901234567890123456789012345678901234567890123456
        'ASCA_SZO_1B_M02_20070116060300Z_20070116074458Z_N_C_20070116075025Z'
        
        """
        self.INSTRUMENT_ID    = self.s[ 0: 4]
        self.PRODUCT_TYPE     = self.s[ 5: 8]
        self.PROCESSING_LEVEL = self.s[ 9:11]
        self.SPACECRAFT_IUD   = self.s[12:15]
        self.SENSING_START    = GeneralTimeClass(self.s[16:31])
        self.SENSING_END      = GeneralTimeClass(self.s[32:47])
        self.PROCESSING_MODE  = self.s[48:49]
        self.DISPOSITION_MODE = self.s[50:51]
        self.PROCESSING_TIME  = GeneralTimeClass(self.s[52:])

        self.SENSING_START.get()
        self.SENSING_END.get()
        self.PROCESSING_TIME.get()

    #-------------------------------------------------
    def put(self):
        """
                   1         2         3         4         5         6
         0123456789012345678901234567890123456789012345678901234567890123456
        'ASCA_SZO_1B_M02_20070116060300Z_20070116074458Z_N_C_20070116075025Z'
        
        """
        self.SENSING_START.put()
        self.SENSING_END.put()
        self.PROCESSING_TIME.put()

        self.s = '_'*67        
        self.s[ 0: 4] =   self.INSTRUMENT_ID   
        self.s[ 5: 8] =   self.PRODUCT_TYPE    
        self.s[ 9:11] =   self.PROCESSING_LEVEL
        self.s[12:15] =   self.SPACECRAFT_IUD  
        self.s[16:31] =   self.SENSING_START.s   
        self.s[32:47] =   self.SENSING_END.s     
        self.s[48:49] =   self.PROCESSING_MODE 
        self.s[50:51] =   self.DISPOSITION_MODE
        self.s[52:67] =   self.PROCESSING_TIME.s 


    

    #-------------------------------------------------
    def display(self,prefix='' ):
        """
        display class entries
        """
	self.get()
	if prefix:
	    self.prefix = prefix

        print self.prefix, self.field,"INSTRUMENT_ID    :",self.INSTRUMENT_ID   
        print self.prefix, self.field,"PRODUCT_TYPE     :",self.PRODUCT_TYPE    
        print self.prefix, self.field,"PROCESSING_LEVEL :",self.PROCESSING_LEVEL
        print self.prefix, self.field,"SPACECRAFT_IUD   :",self.SPACECRAFT_IUD  
        print self.prefix, self.field,"PROCESSING_MODE  :",self.PROCESSING_MODE 
        print self.prefix, self.field,"DISPOSITION_MODE :",self.DISPOSITION_MODE


        self.SENSING_START.display(prefix=self.prefix)
        self.SENSING_END.display(prefix=self.prefix)
        self.PROCESSING_TIME.display(prefix=self.prefix)

    #-------------------------------------------------
    def test(self):
        """
        """
        self.display()
        print '--'
        print 'ok'



        
	


        


#  #]
#=================================================================





#=================================================================
#  #[ TEST SCRIPT
#
if __name__ == '__main__':   #EXECUTABLE
    instance = ProductNameClass('ASCA_SZO_1B_M02_20070116060300Z_20070116074458Z_N_C_20070116075025Z')
    instance.test()
else:
    pass  # importable as a module


#  #]
#=================================================================


