"""
EPS PFS Short Cds Time Class
"""
import datetime

import numpy as np

from .CDSTime import CDSTime, METOP_EPOCH


class ShortCDSTime(CDSTime):

    def unpack(self, buf):
        # 'HI'
        epoch = int(buf.unpack('>H')[0])
        millisec = int(buf.unpack('>I')[0])
        self.value = np.datetime64(
            METOP_EPOCH + datetime.timedelta(days=epoch, milliseconds=millisec)
        )

    def pack(self, buf):
        buf.pack('>H', (self.value - METOP_EPOCH).days)
        buf.pack('>I', (self.value - METOP_EPOCH).microseconds / 1000)
