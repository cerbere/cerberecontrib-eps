# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

long_desc = '''
This package contains the Cerbere extension for EPS format.

'''

requires = [
    #'cerbere>=2.0.0'
]

setup(
    name='cerberecontrib-eps',
    version='0.1',
    url='',
    download_url='',
    license='GPLv3',
    author='Jeff Piolle',
    author_email='jfpiolle@gmail.com',
    description='Cerbere extension for EPS format',
    long_description=long_desc,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPLv3 License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Documentation',
        'Topic :: Utilities',
    ],
    entry_points={
        'cerbere.plugins': [
            'ASCATL1BSZREPSDataset = cerberecontrib_eps.dataset.ascatepsdataset:ASCATL1BSZREPSDataset',
            'ASCATL1BSZFEPSDataset = cerberecontrib_eps.dataset.ascatepsdataset:ASCATL1BSZFEPSDataset',
        ]
    },
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires,
    package_data={'cerberecontrib_eps': ['dataset/pfspy/templates/*.rec']},
)
